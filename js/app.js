angular.module('app',[]).controller('appController', function($http, $scope){
  $scope.cFlag = false;

  $scope.doc1 = [
    {
      req: 'Comply with Government Flow Downs',
      stat: false
    },
    {
      req: 'Use GE Aviation-Approved Suppliers',
      stat: false
    },
    {
      req: 'Use Prime’s ASPL',
      stat: false
    },
    {
      req: 'Service Life + 2 Yrs',
      stat: false
    }
  ];

  $scope.doc2 = [
    {
      req: 'On Time Delivery',
      val: 75
    },
    {
      req: 'Productivity Per Employee',
      val: 100
    },
    {
      req: 'Estimated Market Share',
      val: 25
    },
    {
      req: 'Revenues/Assets',
      val: 50
    }
  ];

  $scope.init = function(){
    $scope.audit1 = false;
    $scope.audit2 = false;
  };

  $scope.changeAudit = function(op){
    if(op === '1'){
        $scope.audit1 = true;
        $scope.audit2 = false;
    }else if(op === '2'){
        $scope.audit1 = false;
        $scope.audit2 = true;
    }else{
        $scope.audit1 = false;
        $scope.audit2 = false;
    }

  };

  $scope.checkAudit = function(value, item){
    if(value === true){
      item.stat = true;
    }else{
      item.stat = false;
    }
  };

  $scope.saveAudit = function(){
    alert("Audit Saved");
  };

});
